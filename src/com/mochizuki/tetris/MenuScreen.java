package com.mochizuki.tetris;

import android.content.Context;
import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MenuScreen implements Screen {

	private float POWER_X, POWER_Y;
	private float SCREEN_WIDTH, SCREEN_HEIGHT;

	private Image m_Background;
	private Image m_Logo;
	private Texture m_TextureBackground;
	private Texture m_BtnTexture;
	private Texture m_TextureLogo;
	private Context m_Context;

	private Stage m_Stage;
	private BitmapFont m_Font;

	private TextButton m_btnNewGame;
	private TextButton m_btnExit;
	private TextButton m_btnResume;

	public MenuScreen(Context context) {
		// TODO Auto-generated constructor stub
		m_Context = context;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		m_BtnTexture.dispose();
		m_TextureBackground.dispose();
		m_TextureLogo.dispose();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		Gdx.gl.glClearColor(0f, 0f, 0f, 0f);

		m_Stage.act(Gdx.graphics.getDeltaTime());
		m_Stage.draw();

	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		Log.i(this.getClass().getSimpleName(),
				"============resume...==========");

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		Log.i(this.getClass().getSimpleName(), "============show...==========");
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		SCREEN_HEIGHT = Gdx.graphics.getHeight();
		POWER_X = SCREEN_WIDTH / 640f;
		POWER_Y = SCREEN_HEIGHT / 960f;

		m_Stage = new Stage(SCREEN_WIDTH, SCREEN_HEIGHT, false);

		m_TextureBackground = new Texture(Gdx.files.internal("background/bg_sky.jpg"));
		m_Background = new Image(new TextureRegion(m_TextureBackground, 320, 480));
		m_Background.setWidth(SCREEN_WIDTH);
		m_Background.setHeight(SCREEN_HEIGHT);
		m_Stage.addActor(m_Background);
		
		m_TextureLogo= new Texture(Gdx.files.internal("background/logoab.png"));
		m_Logo=new Image(new TextureRegion(m_TextureLogo,284,61));
		m_Logo.setX((SCREEN_WIDTH-284f)/2);
		m_Logo.setY(SCREEN_HEIGHT-180f*POWER_Y);
		m_Stage.addActor(m_Logo);
		m_Font = new BitmapFont(Gdx.files.internal("fonts/chnfont.fnt"), false);
		m_Font.setScale(POWER_X);

		// 按钮外观
		TextureRegionDrawable mDrawableBtnUp;
		TextureRegionDrawable mDrawableBtnDown;
		m_BtnTexture = new Texture(
				Gdx.files.internal("buttons/button228x31.png"));
		mDrawableBtnUp = new TextureRegionDrawable(new TextureRegion(
				m_BtnTexture, 0, 0, 107, 31));
		mDrawableBtnDown = new TextureRegionDrawable(new TextureRegion(
				m_BtnTexture, 121, 0, 107, 31));

		TextButton.TextButtonStyle mBtnStyle = new TextButton.TextButtonStyle(
				mDrawableBtnUp, mDrawableBtnDown, mDrawableBtnUp);
		mBtnStyle.font = m_Font;

		// 创建按钮
		m_btnNewGame = new TextButton(
				m_Context.getString(R.string.btn_new_game), mBtnStyle);
		m_btnExit = new TextButton(m_Context.getString(R.string.btn_exit),
				mBtnStyle);
		m_btnResume = new TextButton(m_Context.getString(R.string.btn_resume),
				mBtnStyle);

		m_btnNewGame.setWidth(256f * POWER_X);
		m_btnNewGame.setHeight(64f * POWER_Y);
		m_btnNewGame.setX((SCREEN_WIDTH - m_btnNewGame.getWidth()) / 2);
		m_btnNewGame.setY(84f * POWER_Y);

		m_btnExit.setWidth(m_btnNewGame.getWidth());
		m_btnExit.setHeight(m_btnNewGame.getHeight());
		m_btnExit.setX(m_btnNewGame.getX());
		m_btnExit.setY(20f * POWER_Y);

		m_btnResume.setWidth(m_btnNewGame.getWidth());
		m_btnResume.setHeight(m_btnNewGame.getHeight());
		m_btnResume.setX(m_btnNewGame.getX());
		m_btnResume.setY(148f * POWER_Y);

		m_btnNewGame.addListener(cllistener);
		m_btnExit.addListener(cllistener);
		m_btnResume.addListener(cllistener);

		m_Stage.addActor(m_btnResume);
		m_Stage.addActor(m_btnExit);
		m_Stage.addActor(m_btnNewGame);
		Gdx.input.setInputProcessor(m_Stage);

	}

	// 设置按钮监听程序
	private ClickListener cllistener = new ClickListener() {

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			// TODO Auto-generated method stub
			return super.touchDown(event, x, y, pointer, button);
		}

		@Override
		public void touchUp(InputEvent arg0, float arg1, float arg2, int arg3,
				int arg4) {
			// TODO Auto-generated method stub
			if (arg0.getListenerActor() == m_btnNewGame)
				((MainActivity) m_Context).toMainScreen(true);
			if (arg0.getListenerActor() == m_btnResume)
				((MainActivity) m_Context).toMainScreen(false);
			if (arg0.getListenerActor() == m_btnExit)
				((MainActivity) m_Context).finish();
			super.touchUp(arg0, arg1, arg2, arg3, arg4);
		}
	};
}
